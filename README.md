# The CGI CysLab Hardware Challenge

## Objectif

Contourner le mécanisme de SecureBoot interne pour amorçer le système d'exploitation de son choix

![Armory](https://github.com/usbarmory/usbarmory/wiki/images/armory-mark-two-top.png)

## Restrictions

- Ne pas souder sur la carte ou désouder des composants
- Ne pas flasher un nouveau firmware en mémoire ou réécrire le firmware existant
- Ne pas écraser les fichiers déjà présents sur la carte SD
