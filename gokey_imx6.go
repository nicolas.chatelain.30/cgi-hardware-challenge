package main

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/usbarmory/GoKey/internal/usb"
	"github.com/usbarmory/GoKey/internal/utils"
	"github.com/usbarmory/armory-boot/disk"
	"github.com/usbarmory/armory-boot/exec"
	usbserial "github.com/usbarmory/imx-usbserial"
	usbarmory "github.com/usbarmory/tamago/board/usbarmory/mk2"
	"github.com/usbarmory/tamago/soc/nxp/imx6ul"
	"github.com/usbarmory/tamago/soc/nxp/snvs"
	imxusb "github.com/usbarmory/tamago/soc/nxp/usb"
	"io"

	"strings"

	"log"
	"os"
	"time"
)

/* UART RX Buffer */
var rxBuffer bytes.Buffer

/* ext4 parameters */
const (
	ext4offset      = "33554432"
	defaultBootFile = "default.boot"
)

/* DerivationKeys, not encryption keys. Used for derive key from hardware information */
const DerivationKey = "CGISecureBoot"
const DerivationKeyIv = "CGISecureBootIV!"

func init() {
	/* ARM goes brrrrrrrrrrrrrrrr */
	imx6ul.SetARMFreq(imx6ul.FreqMax)
}

func processSerial(uart *usbserial.UART) {
	/* Wait for serial input */
	reader := bufio.NewReader(&rxBuffer)
	var cLine bytes.Buffer
	for {
		time.Sleep(1 * time.Millisecond)
		/* Read a byte from the buffer */
		in, err := reader.ReadByte()
		if err != nil {
			continue
		}
		/* If byte is a CR, process the command */
		if in == 13 {
			/* Send CRLF to UART */
			uart.Write([]byte("\r\n"))
			/* Process command sent over UART */
			handleCommand(uart, cLine.String())
			/* Clear the buffer after command is processed */
			cLine.Reset()
			uart.Write([]byte("> "))
		} else {
			/* Output byte to user over UART */
			uart.WriteByte(in)
			/* Write byte in temporary command buffer */
			cLine.WriteByte(in)
		}

	}
}

func handleCommand(uart *usbserial.UART, command string) {
	if command == "" {
		uart.Write([]byte("CGI CyberSecurity Lab SecureOS Bootloader v0.1\r\nSecure Boot is Enabled\r\nUse 'help' for available commands\r\n"))
	}
	cmds := strings.Split(command, " ")
	switch cmds[0] {
	case "boot":
		/* If boot is provided with args, use the default.boot file on the SDCARD */
		if len(cmds) == 1 {
			/* Detect ext4 partition */
			part, err := disk.Detect(usbarmory.SD, ext4offset)

			if err != nil {
				uart.Write([]byte(fmt.Sprintf("disk detect error: %v\r\n", err)))
				return
			}

			/* Read default boot file */
			defaultBoot, err := part.ReadAll(defaultBootFile)
			if err != nil {
				uart.Write([]byte(fmt.Sprintf("unable to read default boot: %v\r\n", err)))
				return
			}
			bootString := strings.Split(string(defaultBoot), "\n")

			if len(bootString) == 0 {
				uart.Write([]byte("invalid bootstring\r\n"))
				return
			}
			cmds = strings.Split(bootString[0], " ")
			uart.Write([]byte(fmt.Sprintf("booting default: %s\r\n", bootString[0])))
		}
		if len(cmds) < 3 {
			uart.Write([]byte("Usage: boot [binary] [encrypted signature]\r\n"))
			return
		}
		/* Start booting procedure */
		err := boot(uart, cmds[1:])
		if err != nil {
			uart.Write([]byte(err.Error()))
			return
		}

	case "help":
		uart.Write([]byte("Help menu\r\n\r\n" +
			"Available commands: \r\n\r\n" +
			"boot : boot the default system specified in sdcard default.boot\r\n" +
			"boot [binary] [encrypted signature] : boot the specified signed system image\r\n"))
	}
}

func preLaunch() {
	usbarmory.LED("blue", false)
	usbarmory.LED("white", false)
}

/*
params:

	image: file located on sdcard

returns:

	data: file data
	sha256sig: sha256 signature
	err: error
*/
func getImage(image string) ([]byte, string, error) {
	/* Read ext4 partition */
	part, err := disk.Detect(usbarmory.SD, ext4offset)

	if err != nil {
		return nil, "", fmt.Errorf("disk detect error: %v\r\n", err)
	}

	/* Read kernel ELF */
	elf, err := part.ReadAll(image)
	if err != nil {
		return nil, "", err
	}

	/* Process kernel checksum using SHA256 */
	h := sha256.New()
	io.Copy(h, bytes.NewReader(elf))
	bs := h.Sum(nil)
	return elf, hex.EncodeToString(bs), nil
}

func boot(uart *usbserial.UART, command []string) error {
	/* Read parameters */
	image := command[0]
	signature := command[1]

	/* Get secureboot image signature */
	signhash, err := verifySignature(signature)
	if err != nil {
		return err
	}
	if len(signhash) != 2 {
		return fmt.Errorf("invalid signature format\r\n")
	}
	uart.Write([]byte(fmt.Sprintf("Verifying signature of %s (%v)...\r\n", image, signhash)))

	/* Compare the signature hash with target ELF */
	elf, cmpHash, err := getImage(image)
	if err != nil {
		return err
	}

	if cmpHash != signhash[1] {
		return fmt.Errorf("invalid image signature - Expected: %s - Got: %s\r\n", cmpHash, signhash[1])
	}

	/* Load ELF image on allocated DMA region */
	bootbin := &exec.ELFImage{
		Region: mem,
		ELF:    elf,
	}
	if err = bootbin.Load(); err != nil {
		return fmt.Errorf("load error, %v\r\n", err)
	}

	/* Image seems valid, jump to kernel */
	uart.Write([]byte(fmt.Sprintf("cgi-secure-boot: starting unikernel@%.8x in 3 seconds...\r\n", bootbin.Entry())))
	time.Sleep(3 * time.Second)

	/* Jump! */
	if err = bootbin.Boot(preLaunch); err != nil {
		return fmt.Errorf("load error, %v\r\n", err)
	}

	return nil
}

func verifySignature(encSignature string) ([]string, error) {
	/* Decode hex signature */
	buf, err := hex.DecodeString(encSignature)
	if err != nil {
		return nil, fmt.Errorf("Decode error: %v\r\n", err)
	}
	/* Check if data size is correct */
	if len(buf) < aes.BlockSize*2 {
		return nil, fmt.Errorf("Invalid data size\r\n")
	}
	/* IV is stored on the first 16 bytes of the signature */
	iv := make([]byte, aes.BlockSize)
	copy(iv, buf[:aes.BlockSize])
	data := buf[aes.BlockSize:]

	/* Compute the encryption key based on the hardware + DerivationKey */
	if _, err = imx6ul.DCP.DeriveKey([]byte(DerivationKey), []byte(DerivationKeyIv), 0); err != nil {
		return nil, fmt.Errorf("DeriveKey Error: %v\r\n", err)
	}

	/* Decrypt the data buffer */
	if err = imx6ul.DCP.Decrypt(data, 0, iv); err != nil {
		return nil, fmt.Errorf("Decrypt Error: %v\r\n", err)
	}

	/* Strip padding */
	decnopad, err := utils.Unpad(data, aes.BlockSize)
	if err != nil {
		return nil, fmt.Errorf("Padding Error: %v\r\n", err)
	}
	sigdata := string(decnopad)

	/* Return secureboot signature */
	return strings.Split(sigdata, " "), nil
}

func main() {
	device := &imxusb.Device{}

	log.SetFlags(0)
	log.SetOutput(os.Stdout)

	uid := imx6ul.UniqueID()

	/* Configure USB device */
	usb.ConfigureDevice(device, fmt.Sprintf("%X", uid))

	/* Init UART over USB */
	uart := &usbserial.UART{
		Device: device,
		Rx: func(bytes []byte, err error) ([]byte, error) {
			/* Write received UART bytes to buffer */
			rxBuffer.Write(bytes)
			return nil, nil
		},
	}
	if err := uart.Init(); err != nil {
		log.Println(err)
	}
	/* Init DCP Crypto system */
	imx6ul.DCP.Init()

	/* Enable SNVS Security, preventing hardware attacks */
	imx6ul.SNVS.SetPolicy(snvs.SecurityPolicy{
		Clock:             true,
		Temperature:       true,
		Voltage:           true,
		SecurityViolation: true,
		HardFail:          true,
	})

	/* Process serial input */
	go processSerial(uart)

	mode, _ := usbarmory.FrontPortMode()
	port := usbarmory.USB1

	if mode == usbarmory.STATE_NOT_ATTACHED {
		port = usbarmory.USB2
	}

	port.Init()
	port.Device = device
	port.DeviceMode()

	usb.StartInterruptHandler(port)
}
