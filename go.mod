module github.com/usbarmory/GoKey

go 1.21.4

require (
	github.com/usbarmory/armory-boot v0.0.0-20231214220933-25113be568a3
	github.com/usbarmory/imx-usbserial v0.0.0-20230503192150-40b6298b31f8
	github.com/usbarmory/tamago v0.0.0-20231213101626-43fc8af0637b
)

require (
	github.com/dsoprea/go-ext4 v0.0.0-20190528173430-c13b09fc0ff8 // indirect
	github.com/dsoprea/go-logging v0.0.0-20200710184922-b02d349568dd // indirect
	github.com/go-errors/errors v1.0.2 // indirect
	github.com/pierrec/lz4/v4 v4.1.14 // indirect
	github.com/u-root/u-root v0.11.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
)
